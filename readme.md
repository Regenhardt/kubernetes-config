# Anleitung HAW ICC

Quelle: https://icc.informatik.haw-hamburg.de/docs/

Zum Hosting in der ICC braucht man einen Space. Darin können dann Dinge betrieben werden und Speicher (für Datenbanken) reserviert werden.

Zur Datenhaltung ist zunächst ein reservierter Speicherbereich nötig.  
Dieser kann dann an einen Pod gebunden werden, in dem ein DBMS läuft.

Soll ein Pod, z.B. ein Server mit einer WebAPI, aus dem Internet erreichbar sein, braucht man einen Ingress und eine für einen reservierte Subdomain (z.B. example.haw-hamburg.de), dies muss ein Admin machen. Es kann über das Gitlab-Team "Informatik Support" gemacht werden.

## Beispiel WebbApp aus Website + Backend + Datenbank

Anforderung für Beispiel: 

* WebApp
* Backend
* Datenbank

Alles hier muss über eine Shell ausgeführt werden. Für Zugriff auf die ICC muss man generell per VPN mit der HAW verbunden sein. Vor der Konfiguration muss man sich via `icc-login` authentifizieren. Die Datei `icc-login` hier im Repo ist die Linux-Datei.

### Kubenetes Space

Zuerst muss ein kubernetes space eingerichtet werden, in dem alle Services dieses Projektes angelegt werden. Dieser gehört dem Account der den space anlegt. Es können andere Accounts (identifiziert durch die A-Kennung) berechtigt werden. Der Name muss an der HAW eindeutig sein.  
Dazu in der Datei CreateSpace.yml die Variablen ersetzen und diese an kubernetes senden.  

Der Account kann mit folgendem Befehl ermittelt werden:  
`kubectl get account`

Zu ersetzende Variablen:  
- `$space`
- `$a-kennung`

Aufruf:  
`kubectl apply -f CreateSpace.yml`

Mit folgendem Aufruf kann überprüft werden, ob der Space angelegt wurde:  
`kubectl get space $space`

Optional kann mit diesem Aufruf der gerade erstellte Space als Standard gesetzt werden, dann kann bei allen folgenden Aufrufen das -n und der Name des Spaces weggelassen werden:  
`kubectl config set-context --current --namespace=$space`

Jetzt können in dem space Sachen angelegt werden. Der `$space` wird ab jetzt immer wieder benötigt.

Wird keine Datenbank benötigt kann beim Punkt "Backend" weitergemacht werden.

### Speicher für Datenbank

Bevor man eine Datenbank anlegt, muss eine feste Menge Speicher reserviert werden. Man muss sich also überlegen, wie viel Speicher die Applikation brauchen wird, also wie groß die Datenbank höchstens werden kann.  
Hierzu die Variablen in der Datei `ReserveStorage.yml` ersetzen und diese an kubernetes schicken.  
Zu ersetzende Variablen:  
- `$my-storage-claim`
- `$space`
- `$speicher-größe`

Afuruf:  
`kubectl apply -f ReserveStorage.yml`

Von hier wird der Wert von `$my-storage-claim` im nächsten Schritt benötigt.

### Datenbank

Für dieses Beispiel gehen wir davon aus, dass wir ein Backend haben das sich um WebAPI, Logik und Persistenz kümmert. Ein eigener Pod wird für die Datenbank erstellt.  

Damit das Backend auf die Datenbank zugreifen kann erstellen wir zuerst die Datenbank. Hierzu in der Datei `CreateDb.yml` die Variablen ausfüllen. Werte mit Kommentaren aber ohne $ können verändert werden, können aber auch gefahrlos so gelassen werden. Das wieder an Kubernetes schicken.  
Zu ersetzende Variablen:  
- `$space`
- `$password`
- `$db-pod-name`
- `$my-storage-claim`

Aufruf:  
`kubectl apply -f CreateDb.yml`

Von hier wird der Wert von `$db-pod-name` (und, falls verändert, vom Service in CreateDb.yml spec.ports[0].port) im nächsten Schritt benötigt.

### Backend

Das Backend muss in einem Docker-Container in einer Registry zur Verfügung stehen. Wie man den Container mit der Applikation erstellt und diese darin startet ist vom genutzten Framework abhängig und deshalb nicht Teil dieser Anleitung. Ein passendes `Dockerfile` o.ä. für das genutzte Framework kann online gefunden werden.

Die Applikation muss zum Zugriff auf die im vorherigen Schritt erstellte Datenbank deren Zugangsdaten nutzen. Diese können aus der `ConfigMap` ganz oben in der CreateDb.yml ausgelesen werden. Als Host muss der `$db-pod-name` genutzt werden, als Port 5432 (falls er in der CreateDb.yml nicht geändert wurde). Das genaue Format für den connection string hängt vom Framework ab und kann z. B. auf https://www.connectionstrings.com/ nachgesehen werden. Bei Projekten für das Studium ist die Sicherheit noch nicht extrem wichtig, der connection string kann also einfach im Projekt gespeichert werden, je nachdem wo es am besten passt. Nur Geräte innerhalb des Netzes der HAW haben Zugriff auf die Datenbank.

#### Containererstellung

Der Container muss in einer Registry verfügbar sein, auf die die ICC zugreifen kann. Hierzu wird also am Besten die Gitlab-Registry der HAW genutzt. Jedes Gitlab-Projekt hat eine eigene Registry.

Hiermit kann in der Gitlab-Pipeline ein Container erstellt und in die Registry gepusht werden:

```
Package:
  stage: package # Stage muss oben unter "stages" deklariert werden
  only: [master] # Hier wird entschieden, in welchem Branch die Pipeline läuft
  image: docker:dind
  services:
    - docker:dind
  tags:
    - dind
    - docker
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY # Variablen werden hier von Gitlab gesetzt, keine Änderung nötig
    - docker build -t $CI_REGISTRY/$CI_PROJECT_PATH -f ./Dockerfile . # Pfad zum Dockerfile angeben. Der Punkt hinten ist wichtig. Ggf. an eigene Bedürfnisse anpassen.
    - docker push $CI_REGISTRY/$CI_PROJECT_PATH
```

Die Schritte im script-Block können auch lokal ausgeführt werden, dafür muss nur Docker installiert sein. Der Befehl `docker login` muss dann mit der A-Kennung ausgeführt werden. Der Befehl `docker push` muss dann mit dem Namen der Registry und dem Pfad zum Projekt aufgerufen werden. Der Pfad zum Projekt alles was in der URL hinter `git.haw-habmurg.de` steht. Der Name der Registry ist `git.haw-hamburg.de:5005`. Ein kompletter Pfad wäre also zum Beispiel `git.haw-hamburg.de:5005/gruppe-1-4ever/software-engineering-ii/team-2/study-mate`.

Wurde das ausgeführt, ist das Programm fertig verpackt und bereit zum Deploy.

#### Gitlab Zugriff auf die ICC geben

Damit in der Pipeline das Deployment angestoßen werden kann, muss Gitlab Zugriff auf den Kubernetes-Cluster der ICC haben. Dazu wird ein ServiceAccount erstellt, diesem werden Rechte für den Namespace gegeben und der ServiceAccount wird Gitlab zum Zugriff auf den Cluster gegeben.

Zuerst muss ein ServiceAccount erstellt werden. Dazu in der Datei `CreateServiceAccount.yml` die Variablen ersetzen und an Kubernetes schicken.

Zu ersetzende Variablen:
- `$space`
- `$service-account-name`

Aufruf:  
`kubectl apply -f CreateServiceAccount.yml`

Mit folgendem Aufruf kann überprüft werden, ob der ServiceAccount erstellt wurde:  
`kubectl get serviceaccount $service-account-name -n $space`

Dann dem ServiceAccount Rechte für den Namespace geben. Dazu in der Datei `GrantAccessToSpace.yml` die Variablen ersetzen und an Kubernetes schicken.

Zu ersetzende Variablen:  
- `$space`
- `$service-account-name`

Aufruf:  
`kubectl apply -f GrantAccessToSpace.yml`

Mit folgendem Aufruf kann überprüft werden, ob der ServiceAccount die benötigten Rechte hat:  
`kubectl auth can-i create deployments --as system:serviceaccount:$space:$service-account-name`

Mit dem Service Account wurde gleichzeitig ein damit verknüpftes Secret angelegt. Das mit dem Account verknüpfte Secret kann mit dem foglenden Befehl abgerufen werden:  
`kubectl get secret -n $space`

In der Liste muss ein Secret stehen, das mit dem Namen des Service Accounts anfängt, z. B. `study-mate-service-account-token-xxxxx`. Der Name des Secret wird nun gebraucht, zukünftig wird er `$secret` genannt. Beim folgenden Befehl sollten token und ein ca.crt angezeigt werden:  
`kubectl describe secret $secret -n $space`

Das Zertifikat muss im nächsten Schritt in Gitlab eingetragen werden. Dazu muss zunächst das ca.crt aus dem Secret ausgelesen werden. Dazu wird folgender Befehl ausgeführt (Unix):  
`kubectl get secret $secret -n $space -o jsonpath="{['data']['ca\.crt']}" | base64 --decode`

Unter Windows gibt es keinen `base64` Befehl. Dazu kann in der PowerShell folgendes verwendet werden:  
```
set encoded $(kubectl get secret $secret -o jsonpath="{['data']['ca\.crt']}")
[System.Text.Encoding]::ASCII.GetString([Convert]::FromBase64String($encoded))
```

Das Ergebnis sollte "BEGIN CERTIFICATE" und "END CERTIFICATE" enthalten. Das gesamte Ergebnis inklusive der BEGIN/END-Zeilen wird gleich benötigt.

Außerdem wird das Token benötigt. Dazu wird folgender Befehl ausgeführt:  
`kubectl get secret $secret -n $space -o jsonpath="{['data']['token']}" | base64 --decode`  
Bzw. unter Windows PowerShell entsprechend:  
```
set encoded $(kubectl get secret $secret -o jsonpath="{['data']['token']}")
[System.Text.Encoding]::ASCII.GetString([Convert]::FromBase64String($encoded))
```

Das Ergebnis fängt mit ey an und ist eine lange Zeichenkette. Sie stellt ein jwt token dar und kann bei Interesse auf jwt.io dekodiert werden, es wird aber gleich das Token so wie es hier angezeigt wird benötigt.

Nun kann der ICC-Cluster dem Gitlab-Projekt hinzugefügt werden. Dazu im Gitlab-Projekt unter Infrastructure -> Kubernetes clusters oben rechts "Connect a cluster (certificate - deprecated)" auswählen (die HAW arbeitet noch am Upgrade auf Agenten).

Dann folgende Daten eintragen:  
- Name: beliebiger Name, z.B. ICC-Cluster
- Environment Scope bleibt ein *
- API URL: https://icc-proxy.icc.informatik.haw-hamburg.de:8383
- CA certificate: Dekodierter Inhalt von ca.crt aus dem vorherigen Befehl
- Service Token: Dekodierter Inhalt von token aus dem vorherigen Befehl
- RBAC-enabled cluster: Ja
- Gitlab-managed cluster: Nein
- Namespace per environment: Nein

Nun unten bestätigen, dann sollte der Cluster hinzugefügt sein. Soltle es nicht funktioniert haben, erscheint oben am Bildrand eine entsprechende Fehlermeldung.

#### Der ICC Zugriff auf die Gitlab-Registry geben

Damit der ICC-Cluster auf die Gitlab-Registry zugreifen und das Docker-Image abrufen kann, brauchen wir ein Secret, mit dem sich die ICC bei Gitlab autorisieren kann. Dies kommt in Form eines Deploy-Tokens aus Gitlab.

In Gitlab im Projekt unter Setting -> Repository kann ein solches Deploy-Token erstellt werden. Dies muss die Berechtigung "read_registry" haben. Der Token wird dann mit Benutzername+Passwort angezeigt und diese müssen kopiert werden (sie werden nach Erstellung des Tokens nochmal angezeigt).

Dann muss in Kubernetes das Secret angelegt werden:

(Anmerkung: Syntax für Unix-Terminal, bei Windows können so nicht mehrere Zeilen nacheinander geschrieben werden, dort muss alles in eine Zeile)  
```
kubectl create secret docker-registry $secret-name -n $space \
--docker-server=git.haw-hamburg.de:5005 \
--docker-username=$token-benutzername \
--docker-password=$token-passwort
```  

Hier wird der gerade erstellte $secret-name gleich nochmal im nächsten Schritt benötigt.

#### Deployment

Jetzt kann der Container in der ICC hochgefahren werden. Hierzu müssen die Variablen in der `deploy.yml` ausgefüllt werden.  
Zu ersetzende Variablen:  
- `$app-name`
- `$space`
- `$ci-registry`
- `$ci-project-path`
- `$secret-name`

Dann kann die Datei an kubernetes geschickt werden: 

`kubectl apply -f deploy.yml`

Dies kann in der Pipeline gemacht werden um etwaige Veränderungen in der deploy.yml automatisch an kubernetes zu schicken, aber meist wird sich an dieser Datei nichts verändern.  
Nach einer Änderung am Programm und einer Aktualisierung des Containers muss Kubernetes mitgeteilt werden, dass der Pod mit der neuen Version neugestartet werden sollte (Variablen durch eigene Werte ersetzen):

`kubectl -n $space rollout restart deployment deployment/$app-name`

In der Pipeline sähe das dann so aus:

```
Deploy:
  stage: deploy
  only: [master] # Hier muss das gleiche stehen wie im package job
  needs:
    - Package
  image: $CI_REGISTRY/icc/kubectl:v1.19.10
  script:
    - kubectl apply -f deploy.yml
    - kubectl -n $space rollout restart deployment/$app-name
```

Somit läuft die Applikation nun in der ICC. Zum Zugriff aus dem Internet fehlt jetzt noch ein Ingress.

### Ingress

Damit ein Ingress überhaupt funktioniert, muss vorher eine Subdomain registriert werden. Das kann im Informatik support Team in MS Teams. Die komplette Domain muss dann in der Datei `CreateIngress.yml` eingetragen werden.   
Zu ersetzende Variablen:  
- `$ingress-name`
- `$space`
- `$subdomain`
- `$app-name`

Die Datei muss dann an kubernetes geschickt werden:

`kubectl apply -f CreateIngress.yml`

Hier reicht es, dies einmal selbst auszuführen, da sich hier i.d.R. über die Lebenszeit eines Projektes nichts ändert.
